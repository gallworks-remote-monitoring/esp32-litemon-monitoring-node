# ESP32 Basic Furnace Monitoring Node

Written by: Patrick Gall

## Dependencies

- InfluxDB Client: https://github.com/tobiasschuerg/InfluxDB-Client-for-Arduino
- Adafruit BME280: https://github.com/adafruit/Adafruit_BME280_Library 

## Secrets File

This firmware requires a Secrets.h file to be added to the include folder.
This file needs to include the following definitions:
- WIFI_SSID: The default, hardcoded SSID
- WIFI_PASSWORD: The default, hardcoded password
- INFLUXDB_URL
- INFLUXDB_TOKEN
- INFLUXDB_ORG
- INFLUXDB_BUCKET

## Command Outline

| Syntax             | Status      | Description |
| --                 | --          | -- |
| heartbeat          | Implemented | Responds with heartbeat                              |
| setSSID            | Implemented | Opens interactive terminal to set SSID and Passsword |
| telemetry          | Implemented | Send current values of all sensors                   |
| toggleInfluxWrite  | Implemented | Sets whether to write to InfluxCloud or not.         |
| setLocation        | Implemented | |
| setWriteInterval   | Implemented | |
| saveConfig         | Implemented | Write the current configuration to EEPROM            |
| dumpConfig         | Under Dev   | Write the current configuration to the terminal      |
| clearConfig        | Under Dev   | Clears all config, restoring default values          |

### Test Commands
| Syntax             | Status      | Description |
| --                 | --          | -- |
| testEEPROM         | Under Dev   | Writes to and then Reads from each EEPROM addr       |

## InfluxDB Write Structure

TBD

## EEPROM Allocation

Character Arrays are ETX character (0x03) terminated in the EEPROM.

| Address Range | Description           |
| --            | --                    |
| 0-31          | SSID Characters       |
| 32-63         | WIFI Password         |
| 64-123        | Install Location      |
| 124 - 183     | Install Address       |
| 184 - 193     | Write Freq (seconds)  |
| 194           | Influx Write Flag     |
