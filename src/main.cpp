#include "Util.h"
#include "Commands.h"
LazyDragonfly comms;
String incomingData = "";
// Counter for the write interval.
int counter = 0;

#define LINEFEED 10
#define CARRIDGERETURN 13

void setup() {
  // Start Serial Connection
  Serial.begin(115200);
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println(" -- Litemon Node -- ");
  Serial.println("starting...");

  // Add access points from config
  WiFi.mode(WIFI_STA);
  wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD);

  // Attempt to connect to Wifi
  startWifi();
  
  // Start Sensors 
  startBME280();
  
  // Attempt to start Influx
  startInflux();
  
  // Start EEPROM 
  EEPROM.begin(195);

  // Read Config from EEPROM
  Config_WifiSSID = readStringFromEEPROM(EEPROM_SSID_ADDR);
  Serial.print("SSID: ");
  Serial.println(Config_WifiSSID);
  Config_WifiPassword = readStringFromEEPROM(EEPROM_PASSWORD_ADDR);
  Serial.print("Pass: ");
  Serial.println(Config_WifiPassword);
  Config_Address = readStringFromEEPROM(EEPROM_ADDRESS_ADDR);
  Serial.print("Address: " );
  Serial.println(Config_Address);
  Config_Location = readStringFromEEPROM(EEPROM_LOCATION_ADDR);
  Serial.print("Location: ");
  Serial.println(Config_Location);
  Config_Write_Interval = readUint16FromEEPROM(EEPROM_WRITE_FREQ_ADDR);
  Serial.print("Freq: ");
  Serial.println(Config_Write_Interval);
  Config_WriteFlag = readFlagFromEEPROM(EEPROM_INFLUX_WRITE_FLAG_ADDR);
  Serial.print("Flag: ");
  if(Config_WriteFlag) {
    Serial.println("True");
  } else {
    Serial.println("False");
  }
  Serial.println("Config Read from EEPROM");

  sensor.addTag("device", DEVICE);
  sensor.addTag("SSID", WiFi.SSID());

  bmeHumidty.addTag("Device", "LiteMon");
  bmeHumidty.addTag("Measurement", "Humidity");

  bmePressure.addTag("Device", "LiteMon");
  bmePressure.addTag("Measurement", "AirPressure");

  bmeTemp.addTag("Device", "LiteMon");
  bmeTemp.addTag("Measurement", "LoopTemp");
  

  // Setup Commands
  comms.addCommand((LazyDragonflyCommandInterface*) new heartbeatCommand());
  // Influx Config
  comms.addCommand((LazyDragonflyCommandInterface*) new setAddressCommand());

  comms.addCommand((LazyDragonflyCommandInterface*) new setInstallLocationCommand());
  // Influx Write Interval
  comms.addCommand((LazyDragonflyCommandInterface*) new setWriteIntervalCommand());
  // TODO Influx Diagnostics
  // Influx Toggle
  comms.addCommand((LazyDragonflyCommandInterface*) new setInfluxWriteCommand());
  // Wifi Credentials Setup
  comms.addCommand((LazyDragonflyCommandInterface*) new setSSIDCommand());
  // Live Data Read - Telemetry Command
  comms.addCommand((LazyDragonflyCommandInterface*) new getTelemetryCommand());
  // Save Config
  comms.addCommand((LazyDragonflyCommandInterface*) new saveConfigCommand());
  // Dump Config
  comms.addCommand((LazyDragonflyCommandInterface*) new dumpConfigCommand());
  // Dump EEPROM Config
  comms.addCommand((LazyDragonflyCommandInterface*) new dumpEEPROMCommand());

  // TEST FUNCTIONS

  comms.addCommand((LazyDragonflyCommandInterface*) new testEEPROMCommand());
  
  
  // TODO Read Config from EEPROM Here!

}
void loop() {

  // ####################################################################################
  // CHECK COMM PORT FOR INSTRUCTIONS
  // ####################################################################################
  char buff;
  while(Serial.available()) {
    buff = Serial.read();
    if(buff != (char)CARRIDGERETURN && buff != (char)LINEFEED) { // Not a termination
      incomingData += (char)buff;
    }
    else { // Yes a Termination
      if(incomingData.compareTo("") != 0) { // If there is something in incomingData, then try to run it.
        Serial.println();
        Serial.println("Running: " + incomingData);
        while(Serial.available()) { Serial.read();}
        comms.run(incomingData);
        incomingData = "";  
      }
    }
  }



  // ####################################################################################
  // WRITE DATA
  // ####################################################################################
  
  // Check WiFi connection and reconnect if needed
  if (wifiMulti.run() != WL_CONNECTED) {
    Serial.println("Wifi connection lost");
  }


  // ####################################################################################
  // PERFORM DELAY
  // ####################################################################################
  counter++;
  delay(1000);
  if(counter > Config_Write_Interval) {
    counter=0;

    if(Config_WriteFlag) {
      // Write pipetemp
      pipeTemp.clearTags();
      pipeTemp.addTag("Device", Config_Device);
      pipeTemp.addTag("Location", Config_Location);
      pipeTemp.addTag("Address", Config_Address);
      pipeTemp.clearFields();
      pipeTemp.addField("LoopTemp", getTemperature(analogRead(tempPin)));
      if (!client.writePoint(pipeTemp)) {
        Serial.print("InfluxDB write failed: ");
        Serial.println(client.getLastErrorMessage());
        Serial.println(pipeTemp.toLineProtocol());
      } else {
        Serial.print("Influx Written: ");
        Serial.println(pipeTemp.toLineProtocol());
      }    
    }
  }

  // sensors_event_t temp_event, pressure_event, humidity_event;
  // bme_temp->getEvent(&temp_event);
  // bme_pressure->getEvent(&pressure_event);
  // bme_humidity->getEvent(&humidity_event);

  // Serial.print(F("Temperature = "));
  // Serial.print(temp_event.temperature);
  // Serial.println(" *C");

  // Serial.print(F("Humidity = "));
  // Serial.print(humidity_event.relative_humidity);
  // Serial.println(" %");

  // Serial.print(F("Pressure = "));
  // Serial.print(pressure_event.pressure);
  // Serial.println(" hPa");
    
  // Serial.println("Waiting 1 second");
}