#include "LazyDragonfly.h"
#include "Util.h"



/**
 * @brief Heartbeat Command
 * 
 */
class heartbeatCommand : public LazyDragonflyCommandInterface {
    public:
    heartbeatCommand() : LazyDragonflyCommandInterface("heartbeat") {
        
    }
    String toString() {
        return "Heartbeat Command";
    }
    void execute() {
        Serial.println("Litemon Node Heartbeat");
    }
};

class setSSIDCommand : public LazyDragonflyCommandInterface {
    public:
    setSSIDCommand() : LazyDragonflyCommandInterface("setSSID") {

    }
    String toString() {
        return "Sets SSID for main WIFI";
    }
    void execute() {
        Serial.flush();
        Serial.println(" --- SSID & Password Setter --- ");
        // Loop Control Variables
        String newSSID = setStringParamFromUser("Please enter the desired SSUID: ");
        String newPassword = setStringParamFromUser("Please enter the desired Password: ");
        wifiMulti.addAP(newSSID.c_str(), newPassword.c_str());

        // Update Global Config Variables
        Config_WifiSSID = newSSID;
        Config_WifiPassword = newPassword;

        Serial.println("New Access Point Added. Testing Connection");
        startWifi();
        Serial.print("Current Using: ");
        Serial.println(WiFi.SSID());
        Serial.print("IP: ");
        Serial.println(WiFi.localIP());
        Serial.println("Returning to main menu");
    }
};


class getTelemetryCommand : public LazyDragonflyCommandInterface {
public:
    getTelemetryCommand() : LazyDragonflyCommandInterface("telemetry") {

    }
    String toString() {
        return "Retrieves Telemetry Snapshot";
    }
    void execute() {
        // Report SubSystem Status
        if(wifiStatus) {
            Serial.println("Wifi UP");
            Serial.print("Current Using: ");
            Serial.println(WiFi.SSID());
            Serial.print("IP: ");
            Serial.println(WiFi.localIP());
        } else {
            Serial.println("Wifi DOWN");
        }
        if(influxStatus) {
            Serial.println("Influx UP");
            Serial.println(client.getServerUrl());
        } else {
            Serial.println("Influx DOWN");
        }
        if(bmeStatus) {
            Serial.println("BME UP");
        } else {
            Serial.println("BME DOWN");
        }
        // Report Current Sensor Values
        if(bmeStatus) {
            Serial.println("BME 280: ");
        }
        Serial.print("Thermistor Reading: ");
        Serial.println(getTemperature(analogRead(tempPin)));
        Serial.print("Database Write Interval: ");
        Serial.println(Config_Write_Interval);
    }
};

class setInfluxWriteCommand : public LazyDragonflyCommandInterface {
public:
    setInfluxWriteCommand() : LazyDragonflyCommandInterface("toggleInfluxWrite") {

    }
    String toString() {
        return "Sets the Influx Write Flag";
    }
    void execute() {
        String newInfluxWriteStatus = setStringParamFromUser("Write to Influx? (y/n): ");
        if(newInfluxWriteStatus.compareTo("y") == 0) {
            Serial.println("Write to Influx ENABLED");
            Config_WriteFlag = true;
        } else {
            Serial.println("Write to Influx DISABLED");
            Config_WriteFlag = false;
        }
    }
};

class setAddressCommand: public LazyDragonflyCommandInterface {
public:
    setAddressCommand() : LazyDragonflyCommandInterface("setAddress") {

    }
    String toString() {
        return "Sets the Address of the LiteMon Node";
    }
    void execute() {
        String newAddress = setStringParamFromUser("Enter in new Adrress: ");
        if(newAddress != "") {
            Config_Address = newAddress;
        }
        Serial.println("Address Set");
    }
};

class setInstallLocationCommand : public LazyDragonflyCommandInterface {
public:
    setInstallLocationCommand() : LazyDragonflyCommandInterface("setLocation") {

    }
    String toString() {
        return "Sets the Installation Location Tag for the Whole Node";
    }
    void execute() {
        String newInstall = setStringParamFromUser("Enter in new install location name: ");
        if(newInstall != "") {
            Config_Location = newInstall;
        }

        Serial.println("Location Set");
    }
};


class setWriteIntervalCommand : public LazyDragonflyCommandInterface {
public:
    setWriteIntervalCommand() : LazyDragonflyCommandInterface("setWriteInterval") {

    }
    String toString() {
        return "Sets the interval of time for influx writes (seconds): ";
    }
    void execute() {
        String newInterval = setStringParamFromUser("Enter new write interval (seconds): ");
        if(newInterval != "") {
            Config_Write_Interval = newInterval.toInt();
        }
        Serial.print("Write interval set to : ");
        Serial.println(Config_Write_Interval);
    }
};

class saveConfigCommand : public LazyDragonflyCommandInterface {
public:
    saveConfigCommand() : LazyDragonflyCommandInterface("saveConfig") {

    }
    String toString() {
        return "Write Current Configuration to EEPROM";
    }
    void execute() {
        Serial.println("Writing Configuration...");
        // Write SSID
        writeCharArrayToEEPROM(EEPROM_SSID_ADDR, Config_WifiSSID.c_str(),Config_WifiSSID.length());
        // Write Password
        writeCharArrayToEEPROM(EEPROM_PASSWORD_ADDR, Config_WifiPassword.c_str(),Config_WifiPassword.length());
        // Write Location
        writeCharArrayToEEPROM(EEPROM_LOCATION_ADDR, Config_Location.c_str(), Config_Location.length());
        // Write Address
        writeCharArrayToEEPROM(EEPROM_ADDRESS_ADDR, Config_Address.c_str(), Config_Address.length());
        // Write DB Freq
        writeUint16ToEEPROM(EEPROM_WRITE_FREQ_ADDR, (uint16_t) Config_Write_Interval);
        // Write INFLUX Flag
        writeFlagToEEPROM(EEPROM_INFLUX_WRITE_FLAG_ADDR, (uint8_t) Config_WriteFlag);
        completeWrite();
        Serial.println("Configuration Written!");
    }
};

class dumpConfigCommand : public LazyDragonflyCommandInterface {
public:
    dumpConfigCommand() : LazyDragonflyCommandInterface("dumpConfig") {

    }
    String toString() {
        return "Write Current Configuration to Terminal";
    }
    void execute() {
        Serial.println("Current Configuration:");
        Serial.print("SSID: ");
        Serial.println(Config_WifiSSID);
        Serial.print("Pass: ");
        Serial.println(Config_WifiPassword);
        Serial.print("Address: " );
        Serial.println(Config_Address);
        Serial.print("Location: ");
        Serial.println(Config_Location);
        Serial.print("Freq: ");
        Serial.println(Config_Write_Interval);
        Serial.print("Flag: ");
        if(Config_WriteFlag) {
            Serial.println("True");
        } else {
            Serial.println("False");
        }
    }
};

class dumpEEPROMCommand : public LazyDragonflyCommandInterface {
public:
    dumpEEPROMCommand() : LazyDragonflyCommandInterface("dumpEEPROM") {

    }
    String toString() {
        return "Write Current EEPROM Contents to Terminal";
    }
    void execute() {
        Serial.println("Current EEPROM Configuration:");
        Serial.print("SSID: ");
        Serial.println(readStringFromEEPROM(EEPROM_SSID_ADDR));
        Serial.print("Pass: ");
        Serial.println(readStringFromEEPROM(EEPROM_SSID_ADDR));
        Serial.print("Address: " );
        Serial.println(readStringFromEEPROM(EEPROM_SSID_ADDR));
        Serial.print("Location: ");
        Serial.println(readStringFromEEPROM(EEPROM_SSID_ADDR));
        Serial.print("Freq: ");
        Serial.println(readUint16FromEEPROM(EEPROM_WRITE_FREQ_ADDR));
        Serial.print("Flag: ");
        if(readFlagFromEEPROM(EEPROM_INFLUX_WRITE_FLAG_ADDR)) {
            Serial.println("true");
        } else {
            Serial.println("false");
        }
    }
};

  // ####################################################################################
  // TEST INSTRUCTIONS
  // ####################################################################################

class testEEPROMCommand: LazyDragonflyCommandInterface {
public:
    testEEPROMCommand(): LazyDragonflyCommandInterface("testEEPROM") {}
    String toString() {
        return "Writes to and then Reads from the EEPROM";
    }
    void execute() {
        int addr[] = {EEPROM_SSID_ADDR, EEPROM_PASSWORD_ADDR, EEPROM_LOCATION_ADDR, EEPROM_ADDRESS_ADDR, EEPROM_WRITE_FREQ_ADDR, EEPROM_INFLUX_WRITE_FLAG_ADDR};
        String addr_names[] = {"EEPROM_SSID_ADDR", "EEPROM_PASSWORD_ADDR", "EEPROM_LOCATION_ADDR", "EEPROM_ADDRESS_ADDR", "EEPROM_WRITE_FREQ_ADDR", "EEPROM_INFLUX_WRITE_FLAG_ADDR"};
        String string_eeprom_test_values[] = {"TEST_SSID", "TEST_PASSWORD", "TEST_LOCATION", "TEST_ADDR"};
        // ? SSID Password Location and Address Tests
        for(int i = 0; i < 4; i++) {
            // ? Write to EEPROM addr
            Serial.println("Writing to " + addr_names[i] + " with Value: " + string_eeprom_test_values[i]);
            writeCharArrayToEEPROM(addr[i], string_eeprom_test_values[i].c_str(), string_eeprom_test_values[i].length());
            // ? Read from EEPROM addr SSID
            Serial.print("Reading from EEPROM: ");
            Serial.println(readStringFromEEPROM(addr[i]));
        }

        // ? Test Influx Write Interval
        Serial.println("Writing to " + addr_names[4] + " with Value: 23");
        writeUint16ToEEPROM(addr[5], (char)(uint16_t)23);
        Serial.print("Reading from SSID: ");
        Serial.println(readUint16FromEEPROM(addr[5]));

        // ? Test Influx Write Flag
        Serial.println("Writing to " + addr_names[5] + " with Value: false");
        writeFlagToEEPROM(addr[6], (uint8_t)false);
        if(readFlagFromEEPROM(addr[6])) {
            Serial.println("Read True");
        } else {
            Serial.println("Read False");
        }
    }
};