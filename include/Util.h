#pragma once

#ifndef UTIL_H
#define UTIL_H
    #include <Arduino.h>
    #include "Configuration.h"

    // ####################################################################################
    // CONTENTS
    // 1) SERIAL UTILS
    // 2) SUBSYSTEM INITIALIZERS
    // 3) MEMORY ACCESS FUNCTIONS
    // ####################################################################################

    // ####################################################################################
    // SERIAL UTILS
    // ####################################################################################

    String parseLineFromSerial() { // TODO 
        bool receivedResponse = false;
        String result = "";
        while(!receivedResponse) {
            while(Serial.available()) {
                result += (char)Serial.read();
                if(Serial.peek() == 10 || Serial.peek() == 13) {
                    receivedResponse = true;
                    break;
                }
            }
        }
        return result;
    }

    void clearSerial() {
        while(Serial.available()) {
            Serial.read();
        }
    }
    
    String setStringParamFromUser(String prompt){
        bool receivedResponse = false;
        String result ="";
        int attemptCounter = 0;
        const int maxAttempts = 3;
        // Data Variables
        String response;
        while(!receivedResponse) {
            Serial.println(prompt);
            result = parseLineFromSerial();
            clearSerial();
            Serial.println("Received: " + result);
            Serial.println("Correct? (y/n)");
            response = parseLineFromSerial();
            clearSerial();
            if(response == "y") {
                Serial.println("Parameter Set");
                receivedResponse = true;
            }
            else {
                Serial.println("Entry ignored, please enter again");
                attemptCounter++;
                if(attemptCounter > maxAttempts) {
                    return "";
                }
            }
        }
        return result;
    }

    // ####################################################################################
    // SUBSYSTEM INITIALIZERS
    // ####################################################################################

    // TODO Function to try Wifi Connection
    void startWifi() {
        Serial.print("Attempting to connect to WiFi");
        int timeOutCounter = 0;
        while (wifiMulti.run() != WL_CONNECTED) {
            Serial.print(".");
            delay(100);
            timeOutCounter++;
            if(timeOutCounter > 50) { // 50 with 100mS delay -> Approx 5s

            }
        }
        if(wifiMulti.run() != WL_CONNECTED) {
            Serial.println();
            Serial.println("WiFi not available");
            wifiStatus = false;
        }
        else {
            Serial.println();
            wifiStatus = true;
        }
    }

    // TODO Function to check InfluxDB Connection
    void startInflux() {
        Serial.println("Attemping to Connect to InfluxDB");
        // Accurate time is necessary for certificate validation and writing in batches
        // We use the NTP servers in your area as provided by: https://www.pool.ntp.org/zone/
        // Syncing progress and the time will be printed to Serial.
        timeSync(TZ_INFO, "pool.ntp.org", "time.nis.gov");


        // Check server connection
        if (client.validateConnection()) {
            Serial.print("Connected to InfluxDB: ");
            Serial.println(client.getServerUrl());
            influxStatus = true;
        } else {
            Serial.print("InfluxDB connection failed: ");
            Serial.println(client.getLastErrorMessage());
            influxStatus = false;
        }
    }

    void startBME280() {
        if (bme.begin()) {
            Serial.println("BME280 sensor found");
            bmeStatus = true;
        }
        else {
            Serial.println("Could not find a valid BME280 sensor, check wiring!");
            bmeStatus = false;
        }
    }

    // ####################################################################################
    // MEMORY ACCESS
    // ####################################################################################

    void writeCharArrayToEEPROM(int startAddress, const char* data, int length) {
        for(int i = 0; i < length; i++) {
            EEPROM.write(startAddress + i, data[i]);
        }
        EEPROM.write(startAddress + length, 0x03); // End of Text ETX character termination
        EEPROM.commit();
    }
    String readStringFromEEPROM(int startAddress) {
        String result = "";
        for(int i = 0; i < 32; i++) {
            if(EEPROM.read(startAddress + i) != 0x03) {
                result.concat((char)EEPROM.read(startAddress + i));
            } else {
                break;
            }
        }
        return result;
    }
    void writeUint16ToEEPROM(int startAddress, uint16_t data) {
        uint8_t* byteData = (uint8_t*)&data;
        for(int i = 0; i < sizeof(uint16_t); i++) {
            EEPROM.write(startAddress + i, byteData[i]);
        }
        EEPROM.commit();
    }
    uint16_t readUint16FromEEPROM(int startAddress) {
        uint16_t data;
        uint8_t* byteData = (uint8_t*)&data;
        for(int i = 0; i < sizeof(uint16_t); i++) {
            byteData[i] = EEPROM.read(startAddress + i);
        }
        return data;
    }
    void writeFlagToEEPROM(int address, uint8_t value) {
        EEPROM.write(address, value);
        EEPROM.commit();
    }
    bool readFlagFromEEPROM(int address) {
        uint8_t data;
        data = EEPROM.read(address);
        if(data == 0) {
            return false;
        }
        else {
            return true;
        }
    }
    void completeWrite() {
        EEPROM.commit();
    }



#endif // UTIL_H