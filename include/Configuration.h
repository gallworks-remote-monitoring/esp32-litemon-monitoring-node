#pragma once

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

    /**
     * @file Configuration.h
     * @author Patrick Gall (patrick@gallworks.ca)
     * @brief 
     * @version 0.1
     * @date 2023-01-01
     * 
     * @copyright Copyright (c) 2023
     * 
     */

    // ####################################################################################
    // CONTENTS
    // 1)  Arduino Library Includes
    // 2)  System Flags
    // 2b) System Configuration Global Variables
    // 3)  InfluxDB Credentials
    // 4)  Thermistor Calculator
    // 5)  BME280 Sensor Configuration
    // 6)  EEPROM Allocation
    // ####################################################################################



    // ####################################################################################
    // INCLUDE BASE ARDUINO LIBRARIES
    // ####################################################################################

    #include <Arduino.h>
    #include <EEPROM.h>

    // ####################################################################################
    // SYSTEM FLAGS
    // ####################################################################################

    // Status
    bool wifiStatus = false;
    bool influxStatus = false;
    bool bmeStatus = false;

    // ####################################################################################
    // SYSTEM CONFIGURATION GLOBAL VARIABLES
    // ####################################################################################

    String Config_WifiSSID = "default";
    String Config_WifiPassword = "default";
    String Config_Location = "no-config";
    String Config_Address = "no-config"; // TODO Add this to configuration dump
    String Config_Device = "esp32-LiteMon"; // TODO Add this to configuration dump, can be const
    int Config_Write_Interval = 10;
    bool Config_WriteFlag = false;


    // ####################################################################################
    // SETUP FOR INFLUXDB
    // ####################################################################################

    #if defined(ESP32)
    #include <WiFiMulti.h>
    WiFiMulti wifiMulti;
    #define DEVICE "ESP32"
    #elif defined(ESP8266)
    #include <ESP8266WiFiMulti.h>
    ESP8266WiFiMulti wifiMulti;
    #define DEVICE "ESP8266"
    #endif

    #include <InfluxDbClient.h>
    #include <InfluxDbCloud.h>

    #include <Wire.h>
    #include <SPI.h>
    #include <Adafruit_BME280.h>

    #include "Secrets.h"

    // Time zone info
    #define TZ_INFO "UTC-7"

    // Declare InfluxDB client instance with preconfigured InfluxCloud certificate
    InfluxDBClient client(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN, InfluxDbCloud2CACert);

    // Declare Data point
    Point sensor("wifi_status");
    Point bmeHumidty("Humidity");
    Point bmeTemp("Temp");
    Point bmePressure("Pressure");
    Point pipeTemp("Temp");

    // ####################################################################################
    // THERMISTOR CALCULATION
    // ####################################################################################

    // Thermistor Pin Definitions - TODO -> Eventually stored in FLASH
    #define tempPin A2

    // Thermistor Calibration - Thermistor as Pull Up
    #define THERM_BETA 3435
    #define THERM_T_NAUGHT 298.15
    #define THERM_R_NAUGHT 10000
    #define THERM_R_REF 10000
    const double THERM_R_INF = THERM_R_NAUGHT * pow(2.71828,-((double)THERM_BETA/(double)THERM_T_NAUGHT));

    double getTemperature(int raw_ADC) {
        // Display RInf
        // Serial.print("---Using R_Inf as: ");
        // Serial.println(THERM_R_INF);
        // FInd V_OUT
        double vOut;
        vOut = ((double)raw_ADC / 4096.0) * 3.3; // for ESP 4096, for AVR 1024
        // Serial.print("---RAW ADC: ");
        // Serial.println(raw_ADC);
        // Serial.print("---vOUT: ");
        // Serial.println(vOut);
        // Find R_THERM
        double rTherm;
        rTherm = ((3.3 - vOut) * THERM_R_REF) / vOut;
        // Serial.print("---rTherm: ");
        // Serial.println(rTherm);
        // Calculate Temperature
        double temp;
        temp = THERM_BETA / log(rTherm / THERM_R_INF);
        return temp - 273.15;
    }

    // ####################################################################################
    // BME 280 SENSOR CONFIGURATION
    // ####################################################################################

    // BME 280 Config
    Adafruit_BME280 bme; // use I2C interface
    Adafruit_Sensor *bme_temp = bme.getTemperatureSensor();
    Adafruit_Sensor *bme_pressure = bme.getPressureSensor();
    Adafruit_Sensor *bme_humidity = bme.getHumiditySensor();

    // ####################################################################################
    // EEPROM ALLOCATION
    // ####################################################################################

    #define EEPROM_SSID_ADDR 0
    #define EEPROM_PASSWORD_ADDR 32
    #define EEPROM_LOCATION_ADDR 64
    #define EEPROM_ADDRESS_ADDR 124
    #define EEPROM_WRITE_FREQ_ADDR 184
    #define EEPROM_INFLUX_WRITE_FLAG_ADDR 194

#endif //CONFIGURATION_H